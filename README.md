# tamil-fractions

Follow the generic font design workflow. Wherever possible use Free Software/Hardware tools and techniques maximally.

1. DRAW/SKETCH the font from available proofs and proposals happening in the vernacular community
2. SCAN/DIGITIZE the documents
3. CROP/RESIZE, apply basic image correction using RASTER image processing tool
4. TRACE/VECTORIZE the cropped raster glyph
5. Use Nodes, Biezer, Paths, Stroke, Miller, etc... vector editing tools
6. Follow Font/Glyph design metrics
7. Apply the Glyph to Unicode symbols
8. Reiterate the design & Maintain

Follow the general directory structure as in the repository.

Use the template file (0 - template - to start.svg) to begin drawin in inkscape.
The file has the necessary gridlines and guidelines to start.

## Status Codes

* Untouched - File not created
* Working - File creating, someone is working on it
* Waiting for review - Work completed, waiting for review on accuracy, consistent style
* Review finished - This script is ready to be merged into a font, should not touch this anymore
* Completed - Merged into font

## List

* alakku - ஆளாக்கு  - Waiting for review
* arai - அரை - Waiting for review
* arai-kal - அரை-கால் - Waiting for review
* arai-kani - அரை-காணி - Waiting for review
* arai-ma - அரை-மா - Waiting for review
* cevitu - செவிடு - Waiting for review
* kalam - கலம - Waiting for review்
* kalancu - களஞ்சு - Waiting for review
* kani - காணி - Waiting for review
* marakkal1 - மரக்கால்1 - Waiting for review
* marakkal3 - மரக்கால்3 - Waiting for review
* moovikam - மூவிகம் - Waiting for review
* mukkani - முக்காணி - Waiting for review
* mumma - மும்மா - Waiting for review
* muntiri - முந்திரி 
* nalu-ma - நாலுமா - Waiting for review
* nel - நெல்
* panavetai - பணவேட்டை
* palam - பழம்
* pati - படி - Waiting for review
* rentu-ma - இரண்டு-மiா - Waiting for review
* ulakku - உளாக்கு - Waiting for review
* uri - உரி
